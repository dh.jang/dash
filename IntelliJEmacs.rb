# coding: utf-8
cheatsheet do
  title 'IntelliJ + Emacs Keymap'
  docset_file_name 'IntelliJEmacs'
  keyword 'intellij'
  source_url 'https://gitlab.com/dh.jang/dash'
  category do
    id '코드'

    entry do 
      command '⌃], ⌃['
      notes 'Code block end/start'
      name 'Code block end/start'
    end
    entry do 
      command '⌃⌥I, ⌃⌥Q'
      notes 'Auto-Indent Lines'
      name 'Auto-Indent Lines'
    end
    entry do 
      command '⌃⌥W'
      notes '단어선택, 반복하면 선택영역이 넓어진다.'
      name '단어선택'
    end
  end


  category do
    id '네비게이션'

    entry do
      command '⌥⇧G'
      notes 'Go to class'
      name 'Go to class'
    end
    entry do
      command '⌥→, ⌥←'
      notes '탭 이동'
      name '탭 이동'
    end
    entry do
      command '⌥↑, ⌥↓'
      notes '메소드 이동'
      name '메소드 이동'
    end
    entry do
      command '⌃⌥Y'
      notes '동기화'
      name '동기화'
    end
    entry do
      command '⌃⇧A'
      notes 'Find Action'
      name 'Find Action'
    end
    entry do
      command '⌥⇧F'
      notes '즐겨찾기'
      name '즐겨찾기'
    end
  end

  notes <<-'END'
    * ⌘ : Apple
    * ⎋ : ESC
    * ⇧ : Shift
    * ⌫ : Back Space
    * ⌦ : Delete
    * ⌥ : Option
    * ⌃ : Control
    * ←, →, ↑, ↓ : 화살표 키
    END
end

